
import Data.Char
import Data.List
import System.Random 

randomWord :: [String] -> IO String
randomWord ws = do
                randIndex <- (randomRIO (0, (length ws -1)))
                return $ ws !! randIndex 

filterBlack :: [String] -> String -> [String] 
filterBlack wl []     = wl
filterBlack [] _      = []
filterBlack wl (x:xs) = filterBlack (filter (notElem x) wl) xs

filterYellow :: [String] -> String -> [String] 
filterYellow wl []     = wl
filterYellow [] _      = []
filterYellow wl (x:xs) = filterYellow (filter (elem x) wl) xs

filterGreen :: [String] -> [(Char, Int)] -> [String]            
filterGreen wl []     = wl
filterGreen []  _     = []
filterGreen wl (x:xs) = filterGreen filterList xs where filterList = filter (\p -> p !! (snd x) == fst x) wl

filterList :: [String] -> [(Char, Char, Int)] -> Int -> IO()
filterList wl z chances = do
    let wl1 = filterBlack wl bl    where bl = [ b | (a, b, c) <- z, a == 'B'] 
    let wl2 = filterYellow wl1 yl  where yl = [ b | (a, b, c) <- z, a == 'Y']
    let wl3 = filterGreen wl2 [(b, c) | (a, b, c) <- z, a == 'G']
    play wl3 (chances - 1)
    
checkFeedback :: String -> Bool
checkFeedback fb = fb \\ "GYB" == ""

main :: IO()
main = do
    words <- readFile "worrdleWords.txt"
    let wordList = lines words
    play wordList 10

play :: [String] -> Int -> IO()
play wordList chances = do
       if chances == 0 then putStrLn "I LOSE YOU WON"
       else do 
            word <- randomWord wordList
            putStrLn "__________________________________________"
            putStrLn word
            feedback <- getLine
            if feedback == "GGGGG"
            then putStrLn "IF YOU WANT TO LOSE THE GAME PLAY AGAIN" 
            else do
                let z = zip3 feedback word [0..]
                filterList wordList z chances
